<?php

namespace Drupal\paragraphs_form_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

// cspell:ignore formblock
/**
 * Provides a block for node forms.
 *
 * @Block(
 *   id = "formblock_paragraphs",
 *   admin_label = @Translation("Paragraph form"),
 *   provider = "paragraphs",
 *   category = @Translation("Forms")
 * )
 *
 * Note that we set module to node so that blocks will be disabled correctly
 * when the module is disabled.
 */
class ParagraphsFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new ParagraphsFormBlock plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entityFormBuilder
   *   The entity form builder.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entityTypeBundleInfo
   *   The entity type bundle info.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityFormBuilderInterface $entityFormBuilder,
    protected EntityTypeBundleInfo $entityTypeBundleInfo,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);

    $this->entityTypeManager = $entityTypeManager;
    $this->entityFormBuilder = $entityFormBuilder;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity.form_builder'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * Overrides \Drupal\block\BlockBase::settings().
   */
  public function defaultConfiguration() {
    return [
      'type' => NULL,
      'show_help' => FALSE,
    ];
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockForm().
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['type'] = [
      '#title' => $this->t('Paragraph type'),
      '#description' => $this->t('Select the paragraph type whose form will be shown in the block.'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $this->getParagraphTypes(),
      '#default_value' => $this->configuration['type'],
    ];

    return $form;
  }

  /**
   * Get an array of paragraph types.
   *
   * @return array
   *   An array of paragraph types keyed by machine name.
   */
  protected function getParagraphTypes() {
    $options = [];
    $types = $this->entityTypeBundleInfo->getBundleInfo('paragraph');

    foreach ($types as $type => $label) {

      $options[$type] = $label['label'];
    }
    return $options;
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockSubmit().
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('type', $form_state->getValue('type'));
  }

  /**
   * Implements \Drupal\block\BlockBase::build().
   */
  public function build() {
    $build = [];

    $paragraph = $this->entityTypeManager->getStorage('paragraph')->create([
      'type' => $this->configuration['type'],
    ]);

    $build['form'] = $this->entityFormBuilder->getForm($paragraph);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    $access_control_handler = $this->entityTypeManager->getAccessControlHandler('paragraph');

    // ParagraphsAccessControlHandler::createAccess() adds user.permissions
    // as a cache context to the returned AccessResult.
    /** @var \Drupal\Core\Access\AccessResult $result */
    $result = $access_control_handler->createAccess($this->configuration['type'], $account, [], TRUE);

    // Add the paragraph type as a cache dependency.
    $paragraph_type = $this->entityTypeManager->getStorage('paragraphs_type')->load($this->configuration['type']);
    $result->addCacheTags($paragraph_type->getCacheTags());

    return $result;
  }

}
